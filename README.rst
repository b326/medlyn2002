========================
medlyn2002
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/medlyn2002/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/medlyn2002/1.1.0/

.. image:: https://b326.gitlab.io/medlyn2002/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/medlyn2002

.. image:: https://b326.gitlab.io/medlyn2002/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/medlyn2002/

.. image:: https://badge.fury.io/py/medlyn2002.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/medlyn2002

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/medlyn2002/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/medlyn2002/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/medlyn2002/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/medlyn2002/commits/main
.. #}

Data and formalisms from Medlyn et al. (2002)

