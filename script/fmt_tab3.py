"""
Format values of parameters in table 3.
"""
from pathlib import Path

import pandas as pd

from medlyn2002 import pth_clean

df_raw = pd.read_csv("../raw/tab3.csv", sep=";", comment="#")
df = df_raw.set_index(['species']).sort_index()

# correct typo
df.loc['Fagus sylvatica GH', 'k25'] = 117.18

# write table
df = df[sorted(df.columns)]

with open(pth_clean / "tab3.csv", 'w', encoding="utf-8") as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n")
    fhw.write("# \n")
    fhw.write("# XX_std: standard deviation for value in column XX\n")
    fhw.write("# \n")
    fhw.write("# species: species being measured\n")
    fhw.write("# ha: [kJ.mol-1] activation energy for temp dependence\n")
    fhw.write("# hd: [kJ.mol-1] deactivation energy for temp dependence\n")
    fhw.write("# k25: [µmol m-2.s-1] value of Jmax at 25°C\n")
    fhw.write("# k_opt: [µmol m-2.s-1] value of Jmax at t_opt\n")
    fhw.write("# t_opt: [°C] optimum temperature for Jmax\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.2f")
