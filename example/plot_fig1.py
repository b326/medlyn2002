"""
Figure 1
========

Plot dependency on temperature for Kc, Ko, Jmax and Vcmax
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import raw
from medlyn2002.external import kelvin

oi = 210  # [mmol.mol-1] from below eq11

# compute curves from formalisms
records = []
for t_leaf in np.linspace(5, 40, 100):
    tk = kelvin(t_leaf)

    # badger
    kc = raw.eq7_8(tk)
    ko = raw.eq9(tk)
    km_badger = kc * (1 + oi / ko)
    gamma_star_badger = raw.eq13(kc, ko, oi)

    # jordan
    kc = raw.eq10(tk)
    ko = raw.eq11(tk)
    km_jordan = kc * (1 + oi / ko)
    gamma_star_jordan = oi / 2 / raw.eq14(tk)  # typo correction from paper

    # bernacchi
    kc = raw.eq5(tk)
    ko = raw.eq6(tk)
    km_bernacchi = kc * (1 + oi / ko)
    gamma_star_bernacchi = raw.eq12(tk)

    records.append(dict(
        t_leaf=t_leaf,
        km_badger=km_badger,
        km_jordan=km_jordan,
        km_bernacchi=km_bernacchi,
        gamma_star_badger=gamma_star_badger,
        gamma_star_jordan=gamma_star_jordan,
        gamma_star_bernacchi=gamma_star_bernacchi
    ))

df = pd.DataFrame(records).set_index('t_leaf')

# plot data
fig, axes = plt.subplots(1, 2, sharex='all', figsize=(10, 6), squeeze=False)

ax = axes[0, 0]  # km
for name in ('bernacchi', 'badger', 'jordan'):
    ax.plot(df.index, df[f'km_{name}'], label=name)

ax.legend(loc='upper left')
ax.set_ylim(0, 2500)
ax.set_ylabel("km [µmol.mol-1]")

ax = axes[0, 1]  # gamma_star
for name in ('bernacchi', 'badger', 'jordan'):
    ax.plot(df.index, df[f'gamma_star_{name}'], label=name)

ax.legend(loc='upper left')
ax.set_ylim(0, 100)
ax.set_ylabel("gamma_star [µmol.mol-1]")

ax.set_xlim(0, 45)
ax.set_xlabel("temperature [°C]")

fig.tight_layout()
plt.show()
