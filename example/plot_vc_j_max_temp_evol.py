"""
Vcmax and Jmax dependency on temperature
========================================

Table of curves for all species in the article
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import pth_clean, raw
from medlyn2002.external import kelvin

# read data
p_vc_max = pd.read_csv(pth_clean / "tab2_peaked.csv", sep=";", comment="#", index_col=['species'])
p_j_max = pd.read_csv(pth_clean / "tab3.csv", sep=";", comment="#", index_col=['species'])

# compute curves from formalisms
records = []
for t_leaf in np.linspace(5, 40, 100):
    tk = kelvin(t_leaf)

    entry = dict(t_leaf=t_leaf)
    for name, tab in [('vc', p_vc_max), ('j', p_j_max)]:
        for sp, row in tab.iterrows():
            val = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)
            entry[f'{name}_max_{sp}'] = val

    records.append(entry)

df = pd.DataFrame(records).set_index('t_leaf')

# plot data
fig, axes = plt.subplots(5, 4, sharex='all', sharey='all', figsize=(14, 8), squeeze=False)

for ax, sp in zip(axes.flatten(), p_vc_max.index):
    ax.set_title(sp)
    ax.plot(df.index, df[f'vc_max_{sp}'], label="vc_max")
    ax.plot(df.index, df[f'j_max_{sp}'], label="j_max")

axes[0, 0].legend(loc='upper left')

fig.tight_layout()
plt.show()
