"""
Figure 6
========

Plot evolution of saturated light photosynthesis with temperature
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import pth_clean, raw
from medlyn2002.external import kelvin

# read data
p_vc_max = pd.read_csv(pth_clean / "tab2_peaked.csv", sep=";", comment="#", index_col=['species'])
p_j_max = pd.read_csv(pth_clean / "tab3.csv", sep=";", comment="#", index_col=['species'])

alpha = 0.3  # [mol electron.mol photon-1] from p1170 end of Model section
theta = 0.9  # [-] from p1170 end of Model section

ca = 370  # [µmol.mol-1]
ci = ca * 0.7
oi = 210  # [mmol.mol-1] from below eq11

ppfd = 2000  # [µmol photon.m-2.s-1] since light saturating computation
rd = 0.  # 0.82  # [µmol CO2.m-2.s-1] from Harley 1992 table 1

# compute optimal temperature for photosynthesis
records = []
for sp in p_vc_max.index:
    for t_leaf in list(np.linspace(0, 25, 50, endpoint=False)) + list(np.linspace(25, 50, 100)):
        tk = kelvin(t_leaf)

        kc = raw.eq5(tk)
        ko = raw.eq6(tk)
        gamma_star = raw.eq12(tk)

        row = p_vc_max.loc[sp]
        vc_max = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)
        row = p_j_max.loc[sp]
        j_max = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)

        j = raw.eq4(ppfd, j_max, alpha, theta)

        ac = raw.eq2(ci, vc_max, gamma_star, kc, ko, oi)
        aj = raw.eq3(j, ci, gamma_star)

        an = raw.eq1(ac, aj, rd)

        records.append(dict(
            species=sp,
            t_leaf=t_leaf,
            an=an,
            ac=ac,
            aj=aj
        ))

df = pd.DataFrame(records).set_index(['species'])

# plot data
fig, axes = plt.subplots(1, 1, figsize=(12, 7), squeeze=False)

ax = axes[0, 0]
for sp in ['Acer pseudoplatanus', 'Betula pendula OTC', 'Quercus petraea', 'Gossypium hirsutum']:
    df_sp = df.loc[sp].set_index('t_leaf').sort_index()
    an_25 = df_sp.at[25, 'an']

    crv, = ax.plot(df_sp.index, df_sp['an'] / an_25, label=sp)
    ax.plot(df_sp.index, df_sp['ac'] / an_25, '--', color=crv.get_color())

ax.text(0.05, 0.8, "dashed curves correspond to the Ac part of An", ha='left', va='top', transform=ax.transAxes)
ax.legend(loc='upper left', ncol=2)
ax.set_ylabel("an [normalized to 1 at 25°C]")
ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
