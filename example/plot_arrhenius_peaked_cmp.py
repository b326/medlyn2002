"""
Comparison Arrhenius and peaked formalism for vc_max
====================================================

Use both formalisms on same figure
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import pth_clean, raw
from medlyn2002.external import kelvin

# read data
peaked = pd.read_csv(pth_clean / "tab2_peaked.csv", sep=";", comment="#", index_col=['species'])
arrhen = pd.read_csv(pth_clean / "tab2_arrhenius.csv", sep=";", comment="#", index_col=['species'])

# compute curves from formalisms
records = []
for t_leaf in np.linspace(5, 40, 100):
    tk = kelvin(t_leaf)

    entry = dict(t_leaf=t_leaf)
    for sp, row in peaked.iterrows():
        val = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)
        entry[f'vc_max_{sp}_peaked'] = val

    for sp, row in arrhen.iterrows():
        val = raw.eq16(tk, row['k25'], row['ea'] * 1e3)
        entry[f'vc_max_{sp}_arrhen'] = val

    records.append(entry)

df = pd.DataFrame(records).set_index('t_leaf')

# plot data
fig, axes = plt.subplots(5, 4, sharex='all', sharey='all', figsize=(14, 8), squeeze=False)

for ax, sp in zip(axes.flatten(), peaked.index):
    ax.set_title(sp)
    ax.plot(df.index, df[f'vc_max_{sp}_peaked'], label="peaked")
    ax.plot(df.index, df[f'vc_max_{sp}_arrhen'], label="arrhen")

axes[0, 0].legend(loc='upper left')

fig.tight_layout()
plt.show()
