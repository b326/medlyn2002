"""
Figure 2
========

Plot dependency on temperature for Jmax and Vcmax
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import pth_clean, raw
from medlyn2002.external import kelvin

# read data
p_vc_max = pd.read_csv(pth_clean / "tab2_peaked.csv", sep=";", comment="#", index_col=['species'])
p_j_max = pd.read_csv(pth_clean / "tab3.csv", sep=";", comment="#", index_col=['species'])

# compute curves from formalisms
records = []
for t_leaf in np.linspace(5, 40, 100):
    tk = kelvin(t_leaf)

    entry = dict(t_leaf=t_leaf)
    for name, tab in [('vc', p_vc_max), ('j', p_j_max)]:
        for sp, row in tab.iterrows():
            val = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)
            entry[f'{name}_max_{sp}'] = val
            entry[f'{name}_max_norm_{sp}'] = val / row['k25']

    records.append(entry)

df = pd.DataFrame(records).set_index('t_leaf')

# plot data
fig, axes = plt.subplots(1, 2, figsize=(10, 6), squeeze=False)

ax = axes[0, 0]  # vc_max
for sp in ('Acer pseudoplatanus', 'Juglans regia', 'Gossypium hirsutum', 'Pinus sylvestris'):
    ax.plot(df.index, df[f'vc_max_norm_{sp}'], label=sp)

ax.legend(loc='upper left')
ax.set_ylim(0, 4)
ax.set_ylabel("vc_max [normalized to 1 at 25°C]")

ax.set_xlim(15, 35)
ax.set_xlabel("temperature [°C]")

ax = axes[0, 1]  # j_max
for sp in ('Fraxinus excelsior', 'Fagus sylvatica GH', 'Gossypium hirsutum', 'Pinus sylvestris', 'Pinus pinaster'):
    ax.plot(df.index, df[f'j_max_norm_{sp}'], label=sp)

ax.legend(loc='upper left')
ax.set_ylim(0, 2)
ax.set_ylabel("j_max [normalized to 1 at 25°C]")

ax.set_xlim(0, 40)
ax.set_xlabel("temperature [°C]")

fig.tight_layout()
plt.show()
