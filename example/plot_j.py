"""
J evolution
===========

Plot evolution of J with temperature and PPFD
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import pth_clean, raw
from medlyn2002.external import kelvin

# read data
tab = pd.read_csv(pth_clean / "tab3.csv", sep=";", comment="#", index_col=['species'])
pval = tab.loc['Gossypium hirsutum'].to_dict()

ca = 330  # [µmol.mol-1]
oa = 210  # [mmol.mol-1] from below eq11

ci = ca * 0.7
oi = oa
rd = 0.82  # [µmol CO2.m-2.s-1] from Harley 1992 table 1

alpha = 0.3  # [mol electron.mol photon-1] from p1170 end of Model section
theta = 0.9  # [-] from p1170 end of Model section

t_opt = kelvin(pval['t_opt'])
j_max_opt = pval['k_opt']
j_max_ha = pval['ha'] * 1e3  # [kJ.mol-1] to [J.mol-1]
hd = pval['hd'] * 1e3  # [kJ.mol-1] to [J.mol-1]

# compute curves from formalisms
dfs_t = []
for t_leaf in (20, 25, 30, 35, 40):
    tk = kelvin(t_leaf)
    kc = raw.eq5(tk)
    ko = raw.eq6(tk)
    gamma_star = raw.eq12(tk)

    j_max = raw.eq18(tk, t_opt, j_max_opt, j_max_ha, hd)

    records = []
    for ppfd in np.linspace(5, 2000, 100):
        j = raw.eq4(ppfd, j_max, alpha, theta)

        records.append(dict(
            t_leaf=t_leaf,
            ppfd=ppfd,
            j=j
        ))

    df = pd.DataFrame(records)

    dfs_t.append([t_leaf, df])

dfs_q = []
for ppfd in (300, 600, 1200, 2000):

    records = []
    for t_leaf in np.linspace(5, 40, 100):
        tk = kelvin(t_leaf)
        kc = raw.eq5(tk)
        ko = raw.eq6(tk)
        gamma_star = raw.eq12(tk)

        j_max = raw.eq18(tk, t_opt, j_max_opt, j_max_ha, hd)
        j = raw.eq4(ppfd, j_max, alpha, theta)

        records.append(dict(
            t_leaf=t_leaf,
            ppfd=ppfd,
            j=j
        ))

    df = pd.DataFrame(records)

    dfs_q.append([ppfd, df])

dfs_m = []
for sca in (0.2, 0.5, 1, 1.2):

    records = []
    for t_leaf in np.linspace(5, 40, 100):
        tk = kelvin(t_leaf)
        kc = raw.eq5(tk)
        ko = raw.eq6(tk)
        gamma_star = raw.eq12(tk)

        j_max = raw.eq18(tk, t_opt, j_max_opt * sca, j_max_ha, hd)
        j = raw.eq4(2000, j_max, alpha, theta)

        records.append(dict(
            t_leaf=t_leaf,
            j_max_opt=j_max_opt * sca,
            j=j
        ))

    df = pd.DataFrame(records)

    dfs_m.append([sca, df])

# plot data
fig, axes = plt.subplots(1, 3, sharey='all', figsize=(12, 6), squeeze=False)
ax = axes[0, 0]

for t_leaf, sdf in dfs_t:
    ax.plot(sdf['ppfd'], sdf['j'], label=f"{t_leaf:.0f}")

ax.legend(loc='upper left', title="t_leaf")
ax.set_ylabel("j [µmol electon.m-2.s-1]")
ax.set_xlabel("ppfd [µmol photon.m-2.s-1]")

ax = axes[0, 1]

for ppfd, sdf in dfs_q:
    ax.plot(sdf['t_leaf'], sdf['j'], label=f"{ppfd:.0f}")

ax.legend(loc='upper left', title="ppfd")
ax.set_xlabel("t_leaf [°C]")

ax = axes[0, 2]

for sca, sdf in dfs_m:
    ax.plot(sdf['t_leaf'], sdf['j'], label=f"{sca:.1f}")

ax.legend(loc='upper left', title="j_max_opt * sca")
ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
