"""
Figure 4
========

Plot estimation of optimal temperature for photosynthesis
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from medlyn2002 import pth_clean, raw
from medlyn2002.external import kelvin

# read data
p_vc_max = pd.read_csv(pth_clean / "tab2_peaked.csv", sep=";", comment="#", index_col=['species'])
p_j_max = pd.read_csv(pth_clean / "tab3.csv", sep=";", comment="#", index_col=['species'])

alpha = 0.3  # [mol electron.mol photon-1] from p1170 end of Model section
theta = 0.9  # [-] from p1170 end of Model section

ca = 370  # [µmol.mol-1]
ci = ca * 0.7
oi = 210  # [mmol.mol-1] from below eq11

ppfd = 2500  # [µmol photon.m-2.s-1] since light saturating computation
rd = 0.82  # [µmol CO2.m-2.s-1] from Harley 1992 table 1

# compute optimal temperature for photosynthesis
records = []
for sp in p_vc_max.index:
    for t_leaf in np.linspace(0, 50, 100):
        tk = kelvin(t_leaf)

        kc = raw.eq5(tk)
        ko = raw.eq6(tk)
        gamma_star = raw.eq12(tk)

        row = p_vc_max.loc[sp]
        vc_max = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)
        row = p_j_max.loc[sp]
        j_max = raw.eq18(tk, kelvin(row['t_opt']), row['k_opt'], row['ha'] * 1e3, row['hd'] * 1e3)

        j = raw.eq4(ppfd, j_max, alpha, theta)

        ac = raw.eq2(ci, vc_max, gamma_star, kc, ko, oi)
        aj = raw.eq3(j, ci, gamma_star)

        an = raw.eq1(ac, aj, rd)

        records.append(dict(
            species=sp,
            t_leaf=t_leaf,
            an=an
        ))

df = pd.DataFrame(records)

# plot data
fig, axes = plt.subplots(1, 1, figsize=(12, 7), squeeze=False)

ax = axes[0, 0]

for sp, sdf in tuple(df.groupby('species'))[:10]:  # limit number of curves on screen
    crv, = ax.plot(sdf['t_leaf'], sdf['an'], label=sp)
    ind = sdf['an'].idxmax()
    ax.plot([sdf['t_leaf'].loc[ind]], [sdf['an'].loc[ind]], 'o', color=crv.get_color())

ax.legend(loc='upper left', ncol=2)
ax.set_ylabel("an [µmol CO2.m-2.s-1]")
ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
