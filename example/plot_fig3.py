"""
Figure 3
========

Plot correlation between Jmax and Vcmax at 25 °C
"""
import matplotlib.pyplot as plt
import pandas as pd

from medlyn2002 import pth_clean

# read data
p_vc_max = pd.read_csv(pth_clean / "tab2_peaked.csv", sep=";", comment="#", index_col=['species'])
p_j_max = pd.read_csv(pth_clean / "tab3.csv", sep=";", comment="#", index_col=['species'])

j_vc_ratio = 1.67  # from text below section 'Ratio of Jmax :Vcmax'

# plot data
fig, axes = plt.subplots(1, 1, figsize=(10, 5), squeeze=False)

ax = axes[0, 0]
for i, sp in enumerate(p_vc_max.index):
    if i < 10:
        smb = 'o'
    else:
        smb = 's'
    ax.plot(p_vc_max.loc[sp, 'k25'], p_j_max.loc[sp, 'k25'], smb, label=sp)

ax.plot([0, 120], [0, 120 * j_vc_ratio], '--', color='#aaaaaa')

ax.legend(loc='upper left', ncol=2)
ax.set_ylim(0, 250)
ax.set_ylabel("j_max(25°C) [µmol m-2.s-1]")
ax.set_xlim(0, 120)
ax.set_xlabel("vc_max(25°C) [µmol m-2.s-1]")

fig.tight_layout()
plt.show()
